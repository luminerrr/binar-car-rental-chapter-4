// console.log("Implement servermu disini yak 😝!");

const express = require('express')
const path = require('path')
const app = express()
const port = 8000
const FILE_PATH = path.join(__dirname, '../', 'public')

console.log(`directory name : ${FILE_PATH}`) //checking the directory path for public
//static files
app.use(express.static(FILE_PATH))

app.get('/', (req,res) =>{
    res.sendFile(FILE_PATH + '/index.html')
})

app.get('/cars', (req,res) =>{
    res.sendFile(FILE_PATH + '/cari-mobil.html')
})

app.use('/', (req,res) =>{
    res.status(404);
    res.send('PAGE NOT FOUND')
})

app.listen(port, () => console.info(`app listening on port ${port}`))

